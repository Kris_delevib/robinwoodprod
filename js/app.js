(function () {
    $('.callback, .modal').magnificPopup({
        type: 'inline',
        preloader: false,
        showCloseBtn: false,
    });

    $('.close-modal').click(function(){
        $.magnificPopup.close();
    });

    $('.slider').slick({
        fade: true,
        dots: true,
        slidesToShow: 1,
        nextArrow: '<button type="button" aria-label="Следующий" class="slick-next btn slick-arrow-primary"><svg aria-hidden="true" width="15" height="24" viewBox="0 0 15 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M2 2L12.5 12.5L2 23" stroke="#FF9C45" stroke-width="3"/><path d="M2 2L12.5 12.5L2 23" stroke="#FF9C45" stroke-width="3"/></svg></button>',
        prevArrow: '<button type="button" aria-label="Предыдущий" class="slick-left btn slick-arrow-primary"><svg aria-hidden="true" width="15" height="24" viewBox="0 0 15 24" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M13 2L2.5 12.5L13 23" stroke="#FF9C45" stroke-width="3"/><path d="M13 2L2.5 12.5L13 23" stroke="#FF9C45" stroke-width="3"/></svg></button>',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    arrows: false
                }
            }
        ]
    });

    if ($('.slider-multiple .slide').length > 4) {
        $('.slider-multiple').slick({
            infinite: true,
            slidesToShow: 4,
            slidesToScroll: 4,
            nextArrow: '<button aria-label="Следующий" class="slick-next btn slick-arrow-secondary slick-arrow-position"><svg aria-hidden="true" width="13" height="20" viewBox="0 0 13 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.83337 1.66675L10.5834 10.4167L1.83337 19.1667" stroke="#FF9C45" stroke-width="3"/><path d="M1.83337 1.66675L10.5834 10.4167L1.83337 19.1667" stroke="#FF9C45" stroke-width="3"/></svg></button>',
            prevArrow: '<button aria-label="Предыдущий" class="slick-prev btn slick-arrow-secondary slick-arrow-position"><svg aria-hidden="true" width="13" height="20" viewBox="0 0 13 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11.1666 1.66675L2.41663 10.4167L11.1666 19.1667" stroke="#FF9C45" stroke-width="3"/><path d="M11.1666 1.66675L2.41663 10.4167L11.1666 19.1667" stroke="#FF9C45" stroke-width="3"/></svg></button>',
            responsive: [
                {
                    breakpoint: 1440,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 2,
                        slidesToScroll: 2,
                        arrows: false,
                    }
                },
                {
                    breakpoint: 767,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows: false,
                    }
                },
            ]
        });
    }

    /* slider company logo  */

    if ($('.slider-company .company-logo').length > 8) {
        $('.slider-company').slick({
            infinite: true,
            slidesToShow: 7,
            slidesToScroll: 7,
            dots: true,
            nextArrow: '<button aria-label="Предыдущий" class="slick-next btn slick-arrow-secondary slick-arrow-position"><svg aria-hidden="true" width="13" height="20" viewBox="0 0 13 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.83337 1.66675L10.5834 10.4167L1.83337 19.1667" stroke="#FF9C45" stroke-width="3"/><path d="M1.83337 1.66675L10.5834 10.4167L1.83337 19.1667" stroke="#FF9C45" stroke-width="3"/></svg></button>',
            prevArrow: '<button aria-label="Предыдущий" class="slick-prev btn slick-arrow-secondary slick-arrow-position"><svg aria-hidden="true" width="13" height="20" viewBox="0 0 13 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11.1666 1.66675L2.41663 10.4167L11.1666 19.1667" stroke="#FF9C45" stroke-width="3"/><path d="M11.1666 1.66675L2.41663 10.4167L11.1666 19.1667" stroke="#FF9C45" stroke-width="3"/></svg></button>',
            responsive: [
                {
                    breakpoint: 1366,
                    settings: {
                        slidesToShow: 5,
                        slidesToScroll: 5
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        arrows: false,
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3,
                        arrows: false,
                    }
                },

            ]
        });
    }

    if ($('.slide-feedback').length > 7) {
        $('.slider-feedback').slick({
            infinite: true,
            slidesPerRow: 3,
            centerPadding: '20px',
            rows: 2,
            dots: true,
            nextArrow: '<button aria-label="Следующий" class="slick-next btn slick-arrow-secondary slick-arrow-position"><svg aria-hidden="true" width="13" height="20" viewBox="0 0 13 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M1.83337 1.66675L10.5834 10.4167L1.83337 19.1667" stroke="#FF9C45" stroke-width="3"/><path d="M1.83337 1.66675L10.5834 10.4167L1.83337 19.1667" stroke="#FF9C45" stroke-width="3"/></svg></button>',
            prevArrow: '<button aria-label="Предыдущий" class="slick-prev btn slick-arrow-secondary slick-arrow-position"><svg aria-hidden="true" width="13" height="20" viewBox="0 0 13 20" fill="none" xmlns="http://www.w3.org/2000/svg"><path d="M11.1666 1.66675L2.41663 10.4167L11.1666 19.1667" stroke="#FF9C45" stroke-width="3"/><path d="M11.1666 1.66675L2.41663 10.4167L11.1666 19.1667" stroke="#FF9C45" stroke-width="3"/></svg></button>',
            responsive: [
                {
                    breakpoint: 1400,
                    settings: {
                        slidesPerRow: 2,
                    }
                },
                {
                    breakpoint: 1024,
                    settings: {
                        arrows: false,
                    }
                },
                {
                    breakpoint: 992,
                    settings: {
                        slidesPerRow: 1,
                        rows: 2,
                        arrows: false,
                    }
                }
            ]
        });
    }

    /* slider gallery */

    $('.gallery-slider').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 3,
        dots: false,
        prevArrow: '<button aria-label="Предыдущий" class="slick-prev btn btn--noneborder btn--transparent arrow-simple"><svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"><g><path d="M17 2L6.5 12.5L17 23" stroke="#FF9C45" stroke-width="3"/><path d="M17 2L6.5 12.5L17 23" stroke="#FF9C45" stroke-width="3"/></g></svg></button>',
        nextArrow: '<button aria-label="Следующий" class="slick-next btn btn--noneborder btn--transparent arrow-simple"><svg aria-hidden="true" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none"><g><path d="M7 2L17.5 12.5L7 23" stroke="#FF9C45" stroke-width="3"/><path d="M7 2L17.5 12.5L7 23" stroke="#FF9C45" stroke-width="3"/></g></svg></button>',
        responsive: [
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 2,
                    slidesToScroll: 2,
                }
            }
        ]
    });

    $('.gallery').magnificPopup({
        delegate: 'a',
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
            verticalFit: true,
        },
        gallery: {
            enabled: true
        }
    });

    $('.single-select').select2({
        theme: "material",
        width: '100%',
    });

    $('.select-basis').select2({
        theme: "basis",
        width: '100%',
    });

    /* gallery */
    let galleryCard = $('.gallery-card');

    $('.thumb').on('click', function() {
        loadClickedImage($(this))
    });

    let loadClickedImage = function(el) {
        let id = el.data("thumb-id");
        let image = galleryCard.find(`[data-img-id=${id}]`);
        let currentActiveThumb = $('.thumbs').find('.active');
        let currentActiveCard = $('.image-card').find('.active');

        currentActiveThumb.removeClass('active');
        currentActiveCard.css({opacity: 0});

        el.addClass('active');
        image.css({opacity: 0.0})
           .addClass('active')
           .animate({opacity: 1.0}, 200);
    };

    /* tabs */
    let jsTriggers = document.querySelectorAll('.tabs a');

    jsTriggers.forEach(function(trigger) {
        trigger.addEventListener('click', function(e) {
            e.preventDefault();

            let href = this.getAttribute('href');
            let id = href.substring(href.lastIndexOf('#') + 1);
            let content = document.querySelector(`.tab-content__item[id="${id}"]`);
            let activeTrigger = document.querySelector('.tabs a.active');
            let activeContent = document.querySelector('.tab-content__item.active');

            activeTrigger.classList.remove('active');
            trigger.classList.add('active');

            activeContent.classList.remove('active');
            content.classList.add('active');
        });
    });

    /* radio buttons */

    let radioButtons = document.querySelectorAll('.radio-clicked');

    if (radioButtons) {
        radioButtons.forEach((radio) => {
            radio.addEventListener('click', function (e) {
                e.stopPropagation();
                let content = this.nextElementSibling.nextElementSibling;
                if (content !== null) {
                    content.style = `
                        display: block
                    `;
                }

                let siblings = document.querySelectorAll('.radio-clicked');

                for (let i = 0; i < siblings.length; i++) {
                    if (!siblings[i].checked) {
                        if (siblings[i].nextElementSibling.nextElementSibling !== null) {
                            siblings[i].nextElementSibling.nextElementSibling.style = `
                                display: none
                            `;
                        }
                    }
                }
            });
        })
    }

    /* catalog menu */
    let catalogButton = document.querySelectorAll('.catalog-menu .catalog-menu__link');
    let catalogOpen = 'open';
    let buttonActive = 'active';

    catalogButton.forEach(link => {
        link.addEventListener('click', function(e) {
            e.preventDefault();

            if(!this.nextElementSibling.classList.contains(catalogOpen)) {
                closeCatalog()
            }

            let catalog = this.nextElementSibling;
            this.classList.toggle(buttonActive);
            catalog.classList.toggle(catalogOpen);
        });
    });

    /* catalog menu end */


    // menu basis
    const menuButton = document.querySelector('.hamburger');
    const menuWrap = document.querySelector('.basis-menu-wrap');
    menuButton.addEventListener('click', function() {
        this.classList.toggle('active')
        menuWrap.classList.toggle('active');
    });


    let basketButton = document.querySelectorAll('.basket-open');
    let basketModal = document.querySelector('.basket-modal');
    let basketClose = document.querySelector('.basket-modal .btn--close');
    let overlay = document.querySelector('.cart-overlay');


    basketButton.forEach(button => {
        button.addEventListener('click', (e) => {
            e.preventDefault();
            basketModal.classList.toggle('show');
            overlay.classList.toggle('show');
        });
    });
    basketClose.addEventListener('click', () => {
        basketModal.classList.remove('show');
        overlay.classList.remove('show');
    });


    document.addEventListener('click', function (e) {
        let target = e.target;

        if (!target.closest('.catalog-menu__item')) {
            closeCatalog()
        }


        if (!target.closest('.basket-modal') && !target.closest('.basket-open')) {
            basketModal.classList.remove('show');
            overlay.classList.remove('show');
        }
    });

    function closeCatalog() {
        catalogButton.forEach(btn => {
            btn.classList.remove(buttonActive);

            if (btn.nextElementSibling) {
                btn.nextElementSibling.classList.remove(catalogOpen)
            }
        });
    }


})();